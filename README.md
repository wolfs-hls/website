<img alt="Wolfs HLS" src="static/assets/logo.svg" width="200" />

# [Günter Wolfs GmbH](https://www.wolfs-hls.de)

This repository contains the code of the company's website.
It has been made with Gatsby and Netlify CMS:

- Responsive layout with Chakra UI
- GDPR-compliant contact form with hCaptcha, react-hook-form, and nodemailer (with Gatsby functions)
- GDPR-compliant Vimeo integration
- Integration of images from Netlify CMS with Gatsby Plugin Image
- Block-based editor leveraging rehype react.
- Image gallery and carousel
- Font awesome icons
- React helmet integration for SEO
- Automatic generation of `sitemap.xml`, `robots.txt`, and favicons

## [Created by Mirko Lenz](https://www.mirko-lenz.de)

![Mirko Lenz](https://s.gravatar.com/avatar/f17186937b00dd9e608e5e16d6379878?s=200)

## Copyright

Please note that all files located in the folders `/content` and `/static/assets` may not be used in any form without prior permission from Günter Wolfs GmbH.
