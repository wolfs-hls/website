import type { GatsbyConfig } from "gatsby"
import { camelCase, upperFirst } from "lodash"
import path from "path"

const config: GatsbyConfig = {
  siteMetadata: {
    title: `Günter Wolfs GmbH`,
    description: `Fachbetrieb für Heizung, Sanitär, Lüftung und Klima in Faid bei Cochem.`,
    // author: `Mirko Lenz`,
    siteUrl: `https://www.wolfs-hls.de`,
    phone: `02671 91103`,
    email: `info@wolfs-hls.de`,
    openingHours: `Montag–Freitag\n07:30–17:00 Uhr`,
    address: `Am Kreisel West 14
    Gewerbegebiet
    56814 Faid`,
    captchaSiteKey: process.env.NETLIFY
      ? process.env.CAPTCHA_SITE_KEY
      : "10000000-ffff-ffff-ffff-000000000001",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: path.resolve("content"),
      },
    },
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        defaults: {
          formats: ["auto"],
          // placeholder: `tracedSVG`,
        },
      },
    },
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Wolfs HLS`,
        short_name: `Wolfs HLS`,
        start_url: `/`,
        background_color: `#0e41a4`,
        theme_color: `#0e41a4`,
        display: `minimal-ui`,
        icon: `static/assets/logo.png`,
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-catch-links`,
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          // `gatsby-remark-line-breaks`,
          // {
          //   resolve: `gatsby-remark-images`,
          //   options: {
          //     maxWidth: 800,
          //   },
          // },
        ],
      },
    },
    {
      resolve: `gatsby-transformer-yaml`,
      options: {
        // https://github.com/gatsbyjs/gatsby/blob/master/packages/gatsby-transformer-yaml/src/gatsby-node.js
        typeName: function getType({ node, object, isArray }) {
          const basename = isArray ? node.name : path.basename(node.dir)
          const name = upperFirst(camelCase(`${basename} Yaml`))

          if (name === "PagesYaml" || name === "LeistungenYaml") {
            return "PagesYaml"
          }

          return name
        },
      },
    },
    `@chakra-ui/gatsby-plugin`,
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-nprogress`,
    `gatsby-plugin-netlify`,
    {
      resolve: `gatsby-plugin-robots-txt`,
      options: {
        policy: [
          {
            userAgent: "*",
            allow: "/",
            disallow: "/admin",
          },
        ],
      },
    },
  ],
}

export default config
