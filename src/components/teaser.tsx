import { Box, Grid, Link, Text, VStack } from "@chakra-ui/react"
import { Link as GatsbyLink } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { sortBy } from "lodash"
import React from "react"

interface Node {
  title: string
  icon?: any
  fields: {
    slug: string
  }
  teaser: {
    childImageSharp: {
      gatsbyImageData: any
    }
  }
}

export default function Teaser({ nodes }) {
  return (
    <Grid
      gap={5}
      templateColumns={{
        base: `repeat(1, 1fr)`,
        sm: `repeat(min(${nodes.length}, 2), 1fr)`,
        md: `repeat(min(${nodes.length}, 3), 1fr)`,
      }}
    >
      {sortBy(nodes, `title`).map((node: Node, i: number) => (
        <Link
          as={GatsbyLink}
          to={node.fields.slug}
          key={i}
          borderRadius={25}
          _hover={{ bg: "primary.50" }}
          p={5}
        >
          <VStack spacing={2}>
            <Text fontSize={{ base: "md", lg: "lg" }} fontWeight="bold">
              {node.title}
            </Text>
            <Box
              sx={{
                "& [data-placeholder-image]": { borderRadius: "9999px" },
              }}
            >
              <GatsbyImage
                image={getImage(node.teaser)}
                alt={node.title}
                imgStyle={{ borderRadius: "9999px" }}
              />
            </Box>
          </VStack>
        </Link>
      ))}
    </Grid>
  )
}
