import { extendTheme } from "@chakra-ui/react"

const theme = {
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  colors: {
    primary: {
      50: "#e3efff",
      100: "#b7cefd",
      200: "#89aef6",
      300: "#5b8ef1",
      400: "#2f6dec",
      500: "#1654d2",
      600: "#0e41a4",
      700: "#072f76",
      800: "#011c49",
      900: "#00091d",
    },
    secondary: {
      50: "#ffe1e6",
      100: "#ffb1b9",
      200: "#ff7f8b",
      300: "#ff4c5d",
      400: "#ff1a2f",
      500: "#e60016",
      600: "#b40010",
      700: "#81000a",
      800: "#500004",
      900: "#210000",
    },
  },
}

export default extendTheme(theme)
