import { GatsbyFunctionRequest, GatsbyFunctionResponse } from "gatsby"
import { verify } from "hcaptcha"
import nodemailer from "nodemailer"

const transport = nodemailer.createTransport(
  process.env.NETLIFY
    ? {
        host: process.env.SMTP_SERVER,
        port: process.env.SMTP_PORT,
        auth: {
          user: process.env.SMTP_EMAIL,
          pass: process.env.SMTP_PASSWORD,
        },
      }
    : {
        host: "0.0.0.0",
        port: 1025,
      }
)

const secretKey = process.env.NETLIFY
  ? process.env.CAPTCHA_SECRET_KEY
  : "0x0000000000000000000000000000000000000000"

export default function handler(
  req: GatsbyFunctionRequest,
  res: GatsbyFunctionResponse
) {
  const { captchaToken, sendTo, subject, ...body } = req.body
  const attachments = req.files.map(file => ({
    filename: file.originalname,
    content: file.buffer,
    contentType: file.mimetype,
  }))

  verify(secretKey, captchaToken).then(data => {
    if (data.success) {
      const html =
        `<ul>` +
        Object.entries(body)
          .map(([key, value]) => `<li><b>${key}:</b> ${value}</li>`)
          .join("") +
        `</ul>`

      transport.sendMail(
        {
          from: process.env.SMTP_EMAIL,
          to: process.env.NETLIFY ? [sendTo] : ["info@mirko-lenz.de"],
          subject: subject,
          html: html,
          attachments: attachments,
        },
        function (err, info) {
          if (err) {
            res.json({
              title: "Nachricht konnte nicht gesendet werden",
              description: `Senden Sie bitte die Mail direkt an '${sendTo}'.`,
              status: "error",
            })
          } else {
            res.json({
              title: "Nachricht gesendet",
              description: "Wir werden uns schnellstmöglich bei Ihnen melden.",
              status: "success",
            })
          }
        }
      )
    } else {
      res.json({
        title: "Captcha nicht korrekt",
        description: `Bitte klicken Sie auf die Schaltfläche "Ich bin ein Mensch".`,
        status: "error",
      })
    }
  })
}
