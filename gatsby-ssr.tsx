/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/ssr-apis/
 */

// You can delete this file if you're not using it
import { dom } from "@fortawesome/fontawesome-svg-core"
import { GatsbySSR } from "gatsby"
import React from "react"
import { wrapPageElement as wrap } from "./src/root-wrapper"

// https://github.com/jzabala/gatsby-plugin-fontawesome-css/blob/master/utils.js
// https://github.com/jzabala/gatsby-plugin-fontawesome-css/blob/master/gatsby-ssr.js

export const onRenderBody: GatsbySSR["onRenderBody"] = ({
  setHeadComponents,
}) => {
  setHeadComponents([
    <style
      key="fontawesome-css"
      type="text/css"
      dangerouslySetInnerHTML={{ __html: dom.css() }}
    />,
  ])
}

export const wrapPageElement: GatsbySSR["wrapPageElement"] = wrap
