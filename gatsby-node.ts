/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */
import axios from "axios"
import type { GatsbyNode } from "gatsby"
import { createFilePath, createRemoteFileNode } from "gatsby-source-filesystem"

// https://www.gatsbyjs.com/docs/tutorial/part-seven/
export const onCreateNode: GatsbyNode["onCreateNode"] = async ({
  node,
  getNode,
  actions,
  getCache,
  createNodeId,
  createContentDigest,
  reporter,
}) => {
  const { createNode, createNodeField } = actions
  const nodeType = node.internal.type

  if (nodeType.endsWith(`Yaml`)) {
    const slug = createFilePath({ node, getNode, basePath: `pages` }) // basePath: `blog`
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }

  if (nodeType === "PagesYaml") {
    const mdNodes: Array<any> = []
    const remoteNodes: Array<any> = []

    for (const [i, body] of Object.entries((node.body || []) as Array<any>)) {
      if (body.type === "BodyText") {
        const mdNode = {
          id: createNodeId(`${node.id}+BodyText+${i}`),
          children: [],
          parent: node.id,
          internal: {
            content: body.markdown,
            mediaType: `text/markdown`, // Important!
            contentDigest: createContentDigest(body.markdown),
            type: `BodyMarkdown`,
          },
        }

        createNode(mdNode)
        mdNodes.push(mdNode.id)
      } else if (body.type === "BodyVimeo") {
        const res = await axios.get("https://vimeo.com/api/oembed.json", {
          params: {
            dnt: true,
            url: body.url,
            responsive: true,
            autoplay: true,
          },
        })

        if (res.status === 200) {
          const video = res.data

          const fileNode = await createRemoteFileNode({
            url: video.thumbnail_url,
            parentNodeId: node.id,
            createNode,
            createNodeId,
            getCache,
          })

          if (fileNode) {
            const remoteNode = {
              url: body.url,
              html: video.html,
              title: video.title,
              thumbnail: fileNode.id,
            }

            remoteNodes.push(remoteNode)
          }
        }
      }
    }

    createNodeField({ node, name: "mdNodes", value: mdNodes })
    createNodeField({ node, name: "remoteNodes", value: remoteNodes })
  }
}

export const createResolvers: GatsbyNode["createResolvers"] = ({
  createResolvers,
  getNode,
}) => {
  createResolvers({
    LeistungsbereicheYaml: {
      serviceNodes: {
        type: ["PagesYaml"],
        resolve: async (source, args, context, info) => {
          const { entries } = await context.nodeModel.findAll({
            query: {
              filter: {
                serviceArea: { eq: getNode(source.parent)?.name },
              },
            },
            type: "PagesYaml",
          })

          if (entries) {
            return Array.from(entries)
          }

          return Array()
        },
      },
    },
    PagesYaml: {
      serviceAreaNode: {
        type: "File",
        resolve: (source, args, context, info) => {
          return context.nodeModel.findOne({
            query: {
              filter: {
                relativePath: {
                  eq: `leistungsbereiche/${source.serviceArea}.yml`,
                },
              },
            },
            type: "File",
          })
        },
      },
    },
  })
}

export const createSchemaCustomization: GatsbyNode["createSchemaCustomization"] =
  ({ actions, schema }) => {
    const { createTypes } = actions

    const typeDefs = `
    type BodyImages {
      type: String!
      variant: String!
      images: [BodyImage]
    }
    type BodyImage {
      alt: String
      src: File @fileByRelativePath
    }
    type BodyText {
      type: String!
      variant: String!
      markdown: String
    }
    type BodyMarkdown implements Node {
      id: ID!
    }
    type BodyForm {
      type: String!
      sendTo: String!
      subject: String!
      fields: [BodyFormField]
    }
    type BodyFormField {
      type: String!
      name: String!
      help: String
      required: Boolean
    }
    type BodyMetadata {
      type: String!
    }
    type BodyStaff {
      type: String!
    }
    type BodyServiceAreas {
      type: String!
    }
    type BodyVimeo {
      type: String!
      url: String!
    }
    type BodyRemoteNode {
      thumbnail: File! @link
    }
    type PagesYamlFields {
      mdNodes: [BodyMarkdown]! @link
      remoteNodes: [BodyRemoteNode]!
    }
    type PagesYaml implements Node {
      body: [PageBody]!
      fields: PagesYamlFields
    }
  `

    const pageBody = schema.buildUnionType({
      name: `PageBody`,
      types: [
        `BodyText`,
        `BodyImages`,
        `BodyForm`,
        `BodyStaff`,
        `BodyServiceAreas`,
        `BodyMetadata`,
        `BodyVimeo`,
      ],
      resolveType: value => value.type,
    })

    createTypes(typeDefs)
    createTypes([pageBody])
  }
