// Usage with script tag
const { CMS, initCMS: init } = window

CMS.registerEditorComponent({
  id: "form",
  label: "Formular",
  fields: [
    {
      name: "fields",
      label: "Felder",
      widget: "list",
      fields: [
        { name: "title", label: "Titel", widget: "string" },
        {
          name: "type",
          label: "Typ",
          widget: "select",
          options: [
            {
              value: "text",
              label: "Text (einzeilig)",
            },
            {
              value: "textarea",
              label: "Text (mehrzeilig)",
            },
            {
              value: "email",
              label: "E-Mail",
            },
            {
              value: "number",
              label: "Zahl",
            },
            {
              value: "tel",
              label: "Telefon",
            },
            {
              value: "time",
              label: "Uhrzeit",
            },
            {
              value: "date",
              label: "Datum",
            },
            {
              value: "url",
              label: "URL",
            },
            {
              value: "file",
              label: "Datei",
            },
          ],
        },
        {
          name: "help",
          label: "Ausfüllhilfe",
          widget: "string",
          required: false,
        },
        {
          name: "required",
          label: "Verpflichtend",
          widget: "boolean",
        },
      ],
    },
  ],
  pattern: /(<form.*?<\/form>)/gs,
  fromBlock: function (match) {
    const dom = domRoot(match)

    return {
      title: dom.getAttribute("title"),
      fields: domChildren(dom, "field").map(field => ({
        title: field.getAttribute("title"),
        type: field.getAttribute("type"),
      })),
    }
  },
  toBlock: function (obj) {
    return (
      `<form title="${obj.title}"\n>` +
      obj.fields.map(
        field =>
          `\t<field title="${field.title}" type="${field.type}" help="${field.help}" required="${field.required}"></field>\n`
      ) +
      `</form>`
    )
  },
})

const linkFields = [
  {
    name: "title",
    label: "Name",
    widget: "string",
  },
  {
    name: "icon",
    label: "Symbol",
    widget: "string",
    hint: "Siehe https://fontawesome.com/cheatsheet",
  },
  {
    name: "url",
    label: "URL",
    widget: "string",
    hint: "Entweder eine interne URL im Format '/impressum/' oder eine externe URL im Format 'https://www.google.com'.",
    pattern: [
      // https://mathiasbynens.be/demo/url-regex
      // EXTERNAL | INTERNAL | HOME
      "(^https?://[^s/$.?#].[^s]*$)|(^/.*?/$)|(^/$)",
      "",
    ],
  },
]

const collectionSettings = {
  editor: {
    preview: false,
  },
}

const publicFolder = "../assets"

const body = {
  label: "Inhalte",
  label_singular: "Inhalt",
  name: "body",
  widget: "list",
  collapsed: false,
  types: [
    {
      name: "BodyText",
      label: "Text",
      fields: [
        {
          name: "variant",
          label: "Typ",
          widget: "select",
          options: [
            {
              label: "Standard",
              value: "standard",
            },
            {
              label: "Hervorgehoben",
              value: "highlight",
            },
          ],
        },
        {
          label: "Text",
          name: "markdown",
          widget: "markdown",
          modes: ["rich_text"],
          buttons: [
            "bold",
            "italic",
            "link",
            "heading-two",
            "heading-three",
            "heading-four",
            "heading-five",
            "heading-six",
            "quote",
            "bulleted-list",
            "numbered-list",
          ],
          editor_components: [],
        },
      ],
    },
    {
      name: "BodyImages",
      label: "Bilder",
      fields: [
        {
          name: "variant",
          label: "Typ",
          widget: "select",
          options: [
            {
              label: "Standard",
              value: "standard",
            },
            {
              label: "Gallerie",
              value: "gallery",
            },
            {
              label: "Diashow",
              value: "carousel",
            },
            {
              label: "Diashow mit Gallerie",
              value: "carousel-thumbnails",
            },
          ],
        },
        {
          name: "images",
          label: "Bilder",
          widget: "list",
          fields: [
            { name: "src", label: "Bild", widget: "image" },
            { name: "alt", label: "Title", widget: "string" },
          ],
        },
      ],
    },
    {
      name: "BodyForm",
      label: "Formular",
      fields: [
        {
          name: "sendTo",
          label: "Senden an",
          widget: "string",
        },
        {
          name: "fields",
          label: "Felder",
          widget: "list",
          fields: [
            { name: "name", label: "Name", widget: "string" },
            {
              name: "type",
              label: "Typ",
              widget: "select",
              options: [
                {
                  value: "text",
                  label: "Text (einzeilig)",
                },
                {
                  value: "textarea",
                  label: "Text (mehrzeilig)",
                },
                {
                  value: "email",
                  label: "E-Mail",
                },
                {
                  value: "number",
                  label: "Zahl",
                },
                {
                  value: "tel",
                  label: "Telefon",
                },
                {
                  value: "time",
                  label: "Uhrzeit",
                },
                {
                  value: "date",
                  label: "Datum",
                },
                {
                  value: "url",
                  label: "URL",
                },
                {
                  value: "file",
                  label: "Datei",
                },
              ],
            },
            {
              name: "help",
              label: "Ausfüllhilfe",
              widget: "string",
              required: false,
            },
            {
              name: "required",
              label: "Verpflichtend",
              widget: "boolean",
              required: false,
            },
          ],
        },
      ],
    },
    {
      name: "BodyVimeo",
      label: "Vimeo Video",
      fields: [
        {
          name: "url",
          label: "URL",
          widget: "string",
          hint: "Format: https://vimeo.com/ID",
        },
      ],
    },
    {
      name: "BodyMetadata",
      label: "Metadaten",
      fields: [
        {
          name: "hiddenPlaceholder",
          title: "Platzhalter",
          widget: "hidden",
          default: "",
        },
      ],
    },
    {
      name: "BodyServiceAreas",
      label: "Leistungsbereiche",
      fields: [
        {
          name: "hiddenPlaceholder",
          title: "Platzhalter",
          widget: "hidden",
          default: "",
        },
      ],
    },
    {
      name: "BodyStaff",
      label: "Mitarbeiter",
      fields: [
        {
          name: "hiddenPlaceholder",
          title: "Platzhalter",
          widget: "hidden",
          default: "",
        },
      ],
    },
  ],
}

const productionBackend = {
  backend: {
    name: "gitlab",
    repo: "wolfs-hls/website",
    auth_type: "pkce",
    branch: "main",
    app_id: "43f9b6aad816b3cca9c182591f5c9c3e8364bcee26ba7beb55af1a4e49fde171",
    // commit_messages: {
    //   create: "[skip ci] Create {{collection}} '{{slug}}'",
    //   update: "[skip ci] Update {{collection}} '{{slug}}'",
    //   delete: "[skip ci] Delete {{collection}} '{{slug}}'",
    //   uploadMedia: "[skip ci] Upload '{{path}}'",
    //   deleteMedia: "[skip ci] Delete '{{path}}'",
    //   openAuthoring: "[skip ci] '{{message}}'",
    // },
  },
}

const developmentBackend = {
  backend: {
    name: "git-gateway",
  },
  local_backend: true,
}

const backend =
  location.hostname === "localhost" || location.hostname === "127.0.0.1"
    ? developmentBackend
    : productionBackend

init({
  config: {
    ...backend,
    load_config_file: false,
    locale: "de",
    // publish_mode: "editorial_workflow",
    media_folder: "content/assets",
    public_folder: publicFolder,
    slug: {
      encoding: "ascii",
      clean_accents: true,
    },
    collections: [
      {
        name: "menus",
        label: "Menüs",
        label_singular: "Menü",
        folder: "content/menus",
        extension: "yml",
        summary: "{{title}}",
        create: false,
        delete: false,
        ...collectionSettings,
        fields: [
          {
            name: "title",
            label: "Menü",
            widget: "hidden",
          },
          {
            name: "entries",
            label: "Links",
            label_singular: "Link",
            widget: "list",
            summary: "{{title}} — {{url}}",
            fields: linkFields,
          },
        ],
      },
      {
        name: "services",
        label: "Leistungen",
        label_singular: "Leistung",
        folder: "content/leistungen",
        extension: "yml",
        create: true,
        // media_folder: `${publicFolder}/leistungen`,
        // public_folder: `${publicFolder}/leistungen`,
        ...collectionSettings,
        view_groups: [
          {
            label: "Kategorie",
            field: "category",
          },
        ],
        summary: "{{serviceArea | upper}} — {{title}}",
        fields: [
          {
            label: "Titel",
            name: "title",
            widget: "string",
          },
          {
            label: "Leistungsbereich",
            name: "serviceArea",
            widget: "relation",
            collection: "serviceAreas",
            value_field: "{{slug}}",
            search_fields: ["title"],
            display_fields: ["title"],
          },
          {
            label: "Zusammenfassung",
            name: "summary",
            widget: "string",
            required: false,
          },
          body,
        ],
      },
      {
        name: "serviceAreas",
        label: "Leistungsbereiche",
        label_singular: "Leistungsbereich",
        folder: "content/leistungsbereiche",
        extension: "yml",
        create: true,
        // media_folder: `${publicFolder}/leistungen`,
        // public_folder: `${publicFolder}/leistungen`,
        ...collectionSettings,
        fields: [
          {
            label: "Titel",
            name: "title",
            widget: "string",
          },
          {
            name: "icon",
            label: "Symbol",
            widget: "string",
            hint: "Siehe https://fontawesome.com/cheatsheet",
          },
          {
            name: "teaser",
            label: "Teaser",
            widget: "image",
          },
        ],
      },
      {
        name: "page",
        label: "Seiten",
        label_singular: "Seite",
        folder: "content/pages",
        extension: "yml",
        create: true,
        // media_folder: `${publicFolder}/pages`,
        // public_folder: `${publicFolder}/pages`,
        ...collectionSettings,
        fields: [
          {
            label: "Titel",
            name: "title",
            widget: "string",
          },
          {
            label: "Zusammenfassung",
            name: "summary",
            widget: "string",
            required: false,
          },
          body,
        ],
      },
      {
        name: "staff",
        label: "Mitarbeiter",
        label_singular: "Mitarbeiter",
        folder: "content/mitarbeiter",
        extension: "yml",
        create: false,
        delete: false,
        media_folder: `../assets/mitarbeiter`,
        public_folder: `../assets/mitarbeiter`,
        ...collectionSettings,
        fields: [
          {
            label: "Titel",
            name: "title",
            widget: "hidden",
            default: "Mitarbeiter",
          },
          {
            label: "Kategorien",
            name: "categories",
            widget: "list",
            fields: [
              {
                label: "Kategorie",
                name: "title",
                widget: "string",
              },
              {
                label: "Mitarbeiter",
                name: "employees",
                widget: "list",
                fields: [
                  {
                    label: "Name",
                    name: "title",
                    widget: "string",
                  },
                  {
                    label: "Bild",
                    name: "avatar",
                    widget: "image",
                  },
                  {
                    label: "Rolle",
                    name: "role",
                    widget: "string",
                  },
                  {
                    label: "E-Mail",
                    name: "email",
                    widget: "string",
                    required: false,
                  },
                  {
                    label: "Telefon",
                    name: "phone",
                    widget: "string",
                    required: false,
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
})
